package com.yomahub.demo.proxy.statics;

import com.yomahub.demo.proxy.vo.*;

public class StaticProxyTestMain {

    public static void main(String[] args) {
        Person student = new Student("张三");
        PersonProxy studentProxy = new PersonProxy(student);
        studentProxy.wakeup();
        studentProxy.sleep();

        Person doctor = new Doctor("王教授");
        PersonProxy doctorProxy = new PersonProxy(doctor);
        doctorProxy.wakeup();
        doctorProxy.sleep();

        Animal dog = new Dog("旺旺");
        AnimalProxy dogProxy = new AnimalProxy(dog);
        dogProxy.wakeup();
        dogProxy.sleep();

        Animal cat = new Cat("咪咪");
        AnimalProxy catProxy = new AnimalProxy(cat);
        catProxy.wakeup();
        catProxy.sleep();
    }
}
