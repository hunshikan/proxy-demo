package com.yomahub.demo.proxy.dynamic.javassist;

import com.yomahub.demo.proxy.vo.Cat;
import com.yomahub.demo.proxy.vo.Doctor;
import com.yomahub.demo.proxy.vo.Dog;
import com.yomahub.demo.proxy.vo.Student;

public class DynamicJavassistProxyTestMain {

    public static void main(String[] args) throws Exception{
        JavassitProxy proxy = new JavassitProxy(new Student("张三"));
        Student student = (Student) proxy.getProxy();
        student.wakeup();
        student.sleep();

        proxy = new JavassitProxy(new Doctor("王教授"));
        Doctor doctor = (Doctor) proxy.getProxy();
        doctor.wakeup();
        doctor.sleep();

        proxy = new JavassitProxy(new Dog("旺旺"));
        Dog dog = (Dog) proxy.getProxy();
        dog.wakeup();
        dog.sleep();

        proxy = new JavassitProxy(new Cat("咪咪"));
        Cat cat = (Cat) proxy.getProxy();
        cat.wakeup();
        cat.sleep();
    }
}
